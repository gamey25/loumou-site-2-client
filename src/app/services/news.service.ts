import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getNewsDetailForCustomCard(newsId:number){
    return this.http.get<any>(environment.apiUrl+'/second-website/news-detail-for-home?newsId='+newsId);
  }
  getNextPreviousListNews(url:string){
    return this.http.get<any>(url);
  }

  getNewsDetailsById(newsId:number){
    return this.http.get<any>(environment.apiUrl+'/second-website/news/read-more?newsId='+newsId);
  }
}

