import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }
  getListCommentByNewsId(newsId:number){
    return this.http.get<any>(environment.apiUrl+'/second-website/comments/list-comment-by-news-id?newsId='+newsId);
  }

  postUserComment(dataToSend:any){
    return this.http.post<any>(environment.apiUrl+'/second-website/comments/store-new-comment-with-user-id',dataToSend);
  }
}
