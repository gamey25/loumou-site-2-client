import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExternalVideoService {

  constructor(private http: HttpClient) { }

  getNextPreviousListExternalVideos(url:string){
    return this.http.get<any>(url);
  }
}
