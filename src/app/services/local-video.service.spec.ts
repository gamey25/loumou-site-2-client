import { TestBed } from '@angular/core/testing';

import { LocalVideoService } from './local-video.service';

describe('LocalVideoService', () => {
  let service: LocalVideoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalVideoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
