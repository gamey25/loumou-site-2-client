import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalVideoService {

  constructor(private http: HttpClient) { }

  getNextPreviousListLocalVideos(url:string){
    return this.http.get<any>(url);
  }
}
