import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  constructor(private http: HttpClient) { }

  postLikeComment(dataToSend:any){
    return this.http.post<any>(environment.apiUrl+'/second-website/like/store-new-like-with-user-id',dataToSend);
  }
}
