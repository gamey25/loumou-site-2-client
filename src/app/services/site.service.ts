import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private http: HttpClient) { }

  getAccueilData(){
    return this.http.get<any>(environment.apiUrl+'/second-website/site');
  }

  getSocialLinks(){
    return this.http.get<any>(environment.apiUrl+'/second-website/list-social-link');
  }
}
