import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public static SESSION_STORAGE_KEY: string = 'accessToken';
  private user: any;

  constructor(private http: HttpClient) { }

  public getToken(): any {
    let token: any = sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
    if (!token) {
        throw new Error("no token set , authentication required");
    }

    return sessionStorage.getItem(UserService.SESSION_STORAGE_KEY);
  }

  public storeNewUser(user:any){
    return this.http.post<any>(environment.apiUrl+'/user/store-user',user);
  }

  public defaultUser(loginData:any){
    return this.http.post<any>(environment.apiUrl+'/user/default-login',loginData);
  }
}
