import { TestBed } from '@angular/core/testing';

import { ExternalVideoService } from './external-video.service';

describe('ExternalVideoService', () => {
  let service: ExternalVideoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExternalVideoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
