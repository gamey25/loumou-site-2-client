import { HomeComponent } from './home_component/home/home.component';
import { PageNotFoundComponent } from './custom_component/page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNewsDetailsComponent } from './page-news-details/page-news-details.component';

const routes: Routes = [
  {path: 'home/news/:newsId', title:'Details social News', component: PageNewsDetailsComponent},
  {path: 'home', title:'Accueil social News', component: HomeComponent},
  {path: '', title:'Accueil social News', component: HomeComponent},
  {path: '**', title:'Page not found', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
