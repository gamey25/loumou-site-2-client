import { LocalVideo } from './../../interfaces/models';
import { NewsService } from './../../services/news.service';
import { Component, OnInit } from '@angular/core';
import { SiteService } from 'src/app/services/site.service';
import { LocalVideoService } from 'src/app/services/local-video.service';
import { ExternalVideoService } from 'src/app/services/external-video.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  siteName: string='';
  siteDescription: string='';
  accueilParagraphe:string='';
  listNews:any;
  listNewsZone2:any;
  listNewsZone3:any;
  listTagZone2:any;
  listVideo:any;
  activeNextFirstZone:boolean = false;
  urlNextFirstZone:string = '';
  activepreviousFirstZone:boolean = false;
  urlPreviousFirstZone:string = '';
  numCurrentPageFirstZone:number = 1;

  activeNextFirstVideoZone:boolean = false;
  urlNextFirstVideoZone:string = '';
  activePreviousFirstVideoZone:boolean = false;
  urlPreviousFirstVideoZone:string = '';

  activeNextSecondZone:boolean = false;
  urlNextSecondZone:string = '';
  activepreviousSecondZone:boolean = false;
  urlPreviousSecondZone:string = '';
  numCurrentPageSecondZone:number = 1;

  firstExternalVideo:any;
  secondExternalVideo:any;

  activeNextSecondVideoZone:boolean = false;
  urlNextSecondVideoZone:string = '';
  activePreviousSecondVideoZone:boolean = false;
  urlPreviousSecondVideoZone:string = '';


  activeNextThirdZone:boolean = false;
  urlNextThirdZone:string = '';
  activepreviousThirdZone:boolean = false;
  urlPreviousThirdZone:string = '';
  numCurrentPageThirdZone:number = 1;

  activeNextThirdVideoZone:boolean = false;
  urlNextThirdVideoZone:string = '';
  activePreviousThirdVideoZone:boolean = false;
  urlPreviousThirdVideoZone:string = '';




  chooseVideo:number = this.getRandomInt(3);

  constructor(private siteService: SiteService,
              private newsService: NewsService,
              private localVideoService: LocalVideoService,
              private externalVideoService: ExternalVideoService) { }

  ngOnInit(): void {
    this.siteService.getAccueilData().subscribe((data:any)=>{
      this.numCurrentPageFirstZone = data['news-data']['current_page'];
      this.numCurrentPageSecondZone = data['list-news-zone-2']['listTag']['current_page'];
      this.checkButtonFirstZoneFunction(data['news-data']);
      this.checkButtonFirstZoneVideoFunction(data['local-video']);
      this.checkButtonSecondZoneFunction(data['list-news-zone-2']['listTag']);
      this.checkButtonSecondZoneVideoFunction(data['external-video']);
      this.checkButtonThirdZoneFunction(data['list-news-zone-3'])
      this.checkButtonThirdZoneVideoFunction(data['external-video']);
      this.siteName = data['site-data'][0]['name'];
      this.siteDescription = data['site-data'][0]['description'];
      this.accueilParagraphe = data['accueil-paragraphe'][data['accueil-paragraphe'].length-1]['texte'];
      this.listNews = data['news-data']['data'];
      this.listNewsZone2 = data['list-news-zone-2']['listNews'];
      this.listTagZone2 = data['list-news-zone-2']['listTag'];
      this.listNewsZone3 = data['list-news-zone-3']['data'];
      //this.chooseVideo = this.getRandomInt(3);
      this.listVideo = data['local-video']['data'];
      this.firstExternalVideo = data['external-video']['data'][0];
      this.secondExternalVideo = data['external-video']['data'][1];


      console.log("second data site: ",data);
      console.log("this.chooseVideo: ",this.chooseVideo);

    });
  }



  onclickNextButtonFirstZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlNextFirstZone).subscribe((data)=>{
      this.checkButtonFirstZoneFunction(data['news-data']);
      this.numCurrentPageFirstZone = data['news-data']['current_page'];
      this.listNews = data['news-data']['data'];
      console.log('next list data: ',data);
    });
  }

  onclickPreviousButtonFirstZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlPreviousFirstZone).subscribe((data)=>{
      this.checkButtonFirstZoneFunction(data['news-data']);
      this.numCurrentPageFirstZone = data['news-data']['current_page'];
      this.listNews = data['news-data']['data'];
      console.log('next list data: ',data);
    });
  }

  checkButtonFirstZoneFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activepreviousFirstZone = false;
    }else{
      this.activepreviousFirstZone = true;
      this.urlPreviousFirstZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextFirstZone = false;
    }else{
      this.activeNextFirstZone = true;
      this.urlNextFirstZone = dataNewsData['next_page_url'];
    }
  }

  getRandomInt(max:number) {
    return Math.floor(Math.random() * max);
  }

  onclickNextButtonFirstZoneVideoFunction(){
    this.localVideoService.getNextPreviousListLocalVideos(this.urlNextFirstVideoZone).subscribe((data)=>{
      this.checkButtonFirstZoneVideoFunction(data['local-video']);
      this.listVideo = data['local-video']['data'];
      this.chooseVideo = this.getRandomInt(3);
      console.log('next list video: ',data);
    });
  }

  onclickPreviousButtonFirstZoneVideoFunction(){
    this.localVideoService.getNextPreviousListLocalVideos(this.urlPreviousFirstVideoZone).subscribe((data)=>{
      this.checkButtonFirstZoneVideoFunction(data['local-video']);
      this.listVideo = data['local-video']['data'];
      this.chooseVideo = this.getRandomInt(3);
      console.log('previous list video: ',data);
    });
  }

  checkButtonFirstZoneVideoFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activePreviousFirstVideoZone = false;
    }else{
      this.activePreviousFirstVideoZone = true;
      this.urlPreviousFirstVideoZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextFirstVideoZone = false;
    }else{
      this.activeNextFirstVideoZone = true;
      this.urlNextFirstVideoZone = dataNewsData['next_page_url'];
    }
  }


  onclickNextButtonSecondZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlNextSecondZone).subscribe((data)=>{
      this.checkButtonSecondZoneFunction(data['list-news-zone-2']['listTag']);
      this.numCurrentPageSecondZone = data['list-news-zone-2']['listTag']['current_page'];
      this.listNewsZone2 = data['list-news-zone-2']['listNews'];
      this.listTagZone2 = data['list-news-zone-2']['listTag'];
      console.log('next second list data: ',data);
    });
  }

  onclickPreviousButtonSecondZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlPreviousSecondZone).subscribe((data)=>{
      this.checkButtonSecondZoneFunction(data['list-news-zone-2']['listTag']);
      this.numCurrentPageSecondZone = data['list-news-zone-2']['listTag']['current_page'];
      this.listNewsZone2 = data['list-news-zone-2']['listNews'];
      this.listTagZone2 = data['list-news-zone-2']['listTag'];
      console.log('next second list data: ',data);
    });
  }

  checkButtonSecondZoneFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activepreviousSecondZone = false;
    }else{
      this.activepreviousSecondZone = true;
      this.urlPreviousSecondZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextSecondZone = false;
    }else{
      this.activeNextSecondZone = true;
      this.urlNextSecondZone = dataNewsData['next_page_url'];
    }
  }

  onclickNextButtonSecondZoneVideoFunction(){
    this.externalVideoService.getNextPreviousListExternalVideos(this.urlNextSecondVideoZone).subscribe((data)=>{
      this.checkButtonSecondZoneVideoFunction(data['external-video']);
      this.firstExternalVideo = data['external-video']['data'][0];

      console.log('next list external video: ',data);
    });
  }

  onclickPreviousButtonSecondZoneVideoFunction(){
    this.externalVideoService.getNextPreviousListExternalVideos(this.urlPreviousSecondVideoZone).subscribe((data)=>{
      this.checkButtonSecondZoneVideoFunction(data['external-video']);
      this.firstExternalVideo = data['external-video']['data'][0];

      console.log('previous list external video: ',data);
    });
  }

  checkButtonSecondZoneVideoFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activePreviousSecondVideoZone = false;
    }else{
      this.activePreviousSecondVideoZone = true;
      this.urlPreviousSecondVideoZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextSecondVideoZone = false;
    }else{
      this.activeNextSecondVideoZone = true;
      this.urlNextSecondVideoZone = dataNewsData['next_page_url'];
    }
  }


  onclickNextButtonThirdZoneVideoFunction(){
    this.externalVideoService.getNextPreviousListExternalVideos(this.urlNextThirdVideoZone).subscribe((data)=>{
      this.checkButtonThirdZoneVideoFunction(data['external-video']);

      if(typeof(data['external-video']['data'][1]) != 'undefined'){
        this.secondExternalVideo = data['external-video']['data'][1];
      }


      console.log('next second list external video: ',data);
    });
  }

  onclickPreviousButtonThirdZoneVideoFunction(){
    this.externalVideoService.getNextPreviousListExternalVideos(this.urlPreviousThirdVideoZone).subscribe((data)=>{
      this.checkButtonThirdZoneVideoFunction(data['external-video']);
      if(typeof(data['external-video']['data'][1]) != 'undefined'){
        this.secondExternalVideo = data['external-video']['data'][1];
      }

      console.log('previous second list external video: ',data);
    });
  }

  checkButtonThirdZoneVideoFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activePreviousThirdVideoZone = false;
    }else{
      this.activePreviousThirdVideoZone = true;
      this.urlPreviousThirdVideoZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextThirdVideoZone = false;
    }else{
      this.activeNextThirdVideoZone = true;
      this.urlNextThirdVideoZone = dataNewsData['next_page_url'];
    }
  }


  onclickNextButtonThirdZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlNextThirdZone).subscribe((data)=>{
      this.checkButtonThirdZoneFunction(data['list-news-zone-3']);
      this.numCurrentPageThirdZone = data['list-news-zone-3']['current_page'];
      this.listNewsZone3 = data['list-news-zone-3']['data'];
      console.log('next list data: ',data);
    });
  }

  onclickPreviousButtonThirdZoneFunction(){
    this.newsService.getNextPreviousListNews(this.urlPreviousThirdZone).subscribe((data)=>{
      this.checkButtonThirdZoneFunction(data['list-news-zone-3']);
      this.numCurrentPageThirdZone = data['list-news-zone-3']['current_page'];
      this.listNewsZone3 = data['list-news-zone-3']['data'];
      console.log('next list data: ',data);
    });
  }

  checkButtonThirdZoneFunction(dataNewsData:any){

    if(dataNewsData['prev_page_url'] == null){
      this.activepreviousThirdZone = false;
    }else{
      this.activepreviousThirdZone = true;
      this.urlPreviousThirdZone = dataNewsData['prev_page_url'];
    }

    if(dataNewsData['next_page_url'] == null){
      this.activeNextThirdZone = false;
    }else{
      this.activeNextThirdZone = true;
      this.urlNextThirdZone = dataNewsData['next_page_url'];
    }
  }

}
