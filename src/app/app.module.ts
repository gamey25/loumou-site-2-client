import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IvyCarouselModule} from 'angular-responsive-carousel';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageNotFoundComponent } from './custom_component/page-not-found/page-not-found.component';
import { CustomHeaderComponent } from './custom_component/custom-header/custom-header.component';
import { CustomFooterComponent } from './custom_component/custom-footer/custom-footer.component';
import { HomeComponent } from './home_component/home/home.component';
import { CustomCarousselComponent } from './custom_component/custom-caroussel/custom-caroussel.component';
import { CustomNewsCardComponent } from './custom_component/custom-news-card/custom-news-card.component';
import { CustomLocalNewsComponent } from './custom_component/custom-local-news/custom-local-news.component';
import { PageNewsDetailsComponent } from './page-news-details/page-news-details.component';
import { CustomCardNewsDetailsComponent } from './custom_component/custom-card-news-details/custom-card-news-details.component';
import { CustomCardLeaveCommentComponent } from './custom_component/custom-card-leave-comment/custom-card-leave-comment.component';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    CustomHeaderComponent,
    CustomFooterComponent,
    HomeComponent,
    CustomCarousselComponent,
    CustomNewsCardComponent,
    CustomLocalNewsComponent,
    PageNewsDetailsComponent,
    CustomCardNewsDetailsComponent,
    CustomCardLeaveCommentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    IvyCarouselModule,

  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
