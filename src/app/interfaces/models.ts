


export interface News
{
  add_at:Date,
  created_at:Date,
  id:number,
  site_id:number,
  title:string,
  updated_at:Date
}


export interface ImageNews
{
  add_at:Date,
  created_at:Date,
  id:number,
  news_id:number,
  alt_image:string,
  lien_image:string,
  updated_at:Date
}

export interface Tag{
  add_at:Date,
  created_at:Date,
  id:number,
  news_id:number,
  texte:string,
  updated_at:Date
}

export interface LocalVideo{
  add_at:Date,
  created_at:Date,
  id:number,
  site_id:number,
  alt_video:string,
  lien_video:string,
  updated_at:Date
}
