import { InteractionService } from './../services/interaction.service';
import { NewsService } from './../services/news.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-page-news-details',
  templateUrl: './page-news-details.component.html',
  styleUrls: ['./page-news-details.component.scss']
})
export class PageNewsDetailsComponent implements OnInit {


  toNews:any;
  newsId:number=0;
  constructor(private route: ActivatedRoute,
              private newsService: NewsService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:any)=>{
      this.newsId = params['newsId'];
      this.newsService.getNewsDetailsById(params['newsId']).subscribe((data:any)=>{
        this.toNews = data['to news'];
        console.log("les donnees: ",data);
      });
      //console.log("les parametres de l'url: ",params);
    });
  }



}
