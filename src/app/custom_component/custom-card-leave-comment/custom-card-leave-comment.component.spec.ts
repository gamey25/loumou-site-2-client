import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCardLeaveCommentComponent } from './custom-card-leave-comment.component';

describe('CustomCardLeaveCommentComponent', () => {
  let component: CustomCardLeaveCommentComponent;
  let fixture: ComponentFixture<CustomCardLeaveCommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomCardLeaveCommentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomCardLeaveCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
