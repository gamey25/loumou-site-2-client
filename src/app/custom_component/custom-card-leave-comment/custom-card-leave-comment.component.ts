import { ActivatedRoute } from '@angular/router';
import { CommentService } from './../../services/comment.service';
import { UserService } from './../../services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { InteractionService } from 'src/app/services/interaction.service';

@Component({
  selector: 'app-custom-card-leave-comment',
  templateUrl: './custom-card-leave-comment.component.html',
  styleUrls: ['./custom-card-leave-comment.component.scss']
})
export class CustomCardLeaveCommentComponent implements OnInit {

  userData:any;

  showZone:string = "register";
  showLoginZoneError:boolean = false;
  msgLoginError: string="";
  passwordConfirmationPasswordError: boolean=false;
  showRegisterErrorZone:boolean = false;

  registerForm = this.fb.group({
    name: ['Pseudonyme', [Validators.required, Validators.min(6)]],//, Validators.required
    email: ['Email@gmail.com',  [Validators.email,Validators.required]],//, Validators.required, Validators.email
    password: ['Password',  [Validators.minLength(6),Validators.required]],//, Validators.required, Validators.min(6)
    confirmPassword: ['Password', [Validators.minLength(6),Validators.required]],//, Validators.max(20)
  });

  messageForm = this.fb.group({
    name: ['Pseudonyme', [Validators.required, Validators.min(6)]],//, Validators.required
    message: ['Message', [Validators.required, Validators.min(1)]],//, Validators.required
  });

  loginForm = this.fb.group({
    email: ['Email@gmail.com',  [Validators.email,Validators.required]],//, Validators.required, Validators.email
    password: ['Password',  [Validators.minLength(6),Validators.required]],//, Validators.required, Validators.min(6)
  });

  cookieName:any;
  dataToSend:any;
  listComments:any;
  newsData:any;
  newsId:number=0;
  userId:number=0;
  userName:string = "";
  userEmail:string = "";

  constructor(private cookieService: CookieService,
              private userService: UserService,
              private fb: FormBuilder,
              private commentService: CommentService,
              private route: ActivatedRoute,
              private interactionService: InteractionService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:any)=>{
      this.initComment();
      this.newsId = params['newsId'];
      this.commentService.getListCommentByNewsId(this.newsId).subscribe((dataComment)=>{
        this.listComments = dataComment;
        console.log("list comments: ", this.listComments);
      });
    });
    this.chooseZoneToShow();
  }

  chooseZoneToShow(){
    let user = this.cookieService.get('user-data');
    if(user == ''){
      this.showZone="register";
      console.log("this.cookieService.get: ",typeof(user));
    }else{
      this.showZone="comment";
      this.userData = JSON.parse(this.cookieService.get('user-data'));
    }
  }

  whenClickOnLoginButtonRegisterZone(){
    this.showZone = "login";
  }
  whenClickOnRegisterButtonLogimZone(){
    this.showZone = "register";
  }

  onClickSubmitLoginForm(){
    console.log("login data form: ",this.loginForm.value );
    this.showLoginZoneError = false;
    this.userService.defaultUser(this.loginForm.value).subscribe((data:any)=>{
      console.log("data login: ",data);
      if(data['isSuccess']==false){
        this.showLoginZoneError = true;
        this.msgLoginError = data["error"];
      }else{
        this.cookieService.set('user-data',JSON.stringify(data['user']),{ expires: 1000, sameSite: 'Lax' });
        this.userData = data['user'];
        this.userId = data['user']['id'];
        this.showZone='comment';
        this.interactionService.addNewDataToShare({
          'from':'comment-zone',
          'event':'user-logged-successful',
          'data': data
                                      });
      }
    });
  }

  onClickSubmitRegisterForm(){
    let password = this.registerForm.get('password');
    let confirmPassword = this.registerForm.get('confirmPassword')
    if(password?.value === confirmPassword?.value){
      this.passwordConfirmationPasswordError = false;
      this.showRegisterErrorZone =false;
      console.log("je suis la");
      this.userService.storeNewUser(this.registerForm.value).subscribe((data)=>{
        console.log("data post return is: ",data['isSuccess']);
        if(data['isSuccess'] == true){
          this.showZone = "comment";
          this.cookieService.set('user-data',JSON.stringify(data['user']),{ expires: 1000, sameSite: 'Lax' });
          this.userData = data['user'];
          this.interactionService.addNewDataToShare({
            'from':'comment-zone',
            'event':'user-logged-successful',
            'data': data
                                        });
        }else{
          this.showRegisterErrorZone =true;
        }
      });
    }else{
      this.passwordConfirmationPasswordError = true;
    }

    //console.log("register data form: ",this.registerForm.value );
  }

  onClickSubmitMessageForm(){
    console.log("this.messageForm.value: ", this.messageForm.value);
    console.log("user data: ", this.cookieName);
    let message = this.messageForm.value;
    this.dataToSend = {
      'user-id' : this.userId,
      'news-id' : this.newsId,
      'message' : message?.message
    };
    console.log("this.dataToSend: ", this.dataToSend);
    this.commentService.postUserComment(this.dataToSend).subscribe((data)=>{
      this.listComments = data;
      console.log("post comment back: ",data);
    });
    console.log("this.dataToSend: ", this.dataToSend);
  }

  initComment(){
    if(this.cookieService.get('user-data') == ''){
      this.showZone = 'login';
    }else{
      this.cookieName = JSON.parse(this.cookieService.get('user-data'));
      this.userData = this.cookieName;
      this.userName = this.cookieName['name'];
      this.userEmail = this.cookieName['email'];
      this.userId = this.cookieName["id"];
      this.messageForm.setValue({'name':this.userName,'message':'mon commentaire'});
      this.interactionService.addNewDataToShare({
        'from':'comment-zone',
        'event':'user-logged-successful',
        'data': this.userData
                                    });
      this.showZone = 'comment';
    }
    console.log("user-data: ", this.cookieName);
    //console.log("this.newsId: ",this.newsId);

  }

}
