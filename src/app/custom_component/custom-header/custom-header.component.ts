import { SiteService } from 'src/app/services/site.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-header',
  templateUrl: './custom-header.component.html',
  styleUrls: ['./custom-header.component.scss']
})
export class CustomHeaderComponent implements OnInit {


  siteName:string = '';
  constructor(private siteService: SiteService) { }

  ngOnInit(): void {

    this.siteService.getAccueilData().subscribe((data:any)=>{
      this.siteName = data['site-data'][0]['name'];

    });

  }

}
