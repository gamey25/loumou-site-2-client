import { NewsService } from './../../services/news.service';
import { News, ImageNews, Tag } from './../../interfaces/models';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-news-card',
  templateUrl: './custom-news-card.component.html',
  styleUrls: ['./custom-news-card.component.scss']
})
export class CustomNewsCardComponent implements OnInit {

  @Input()
  newsWithoutDetails: News ={
    add_at:new Date(),
    created_at: new Date(),
    site_id:0,
    title:'',
    id:0,
    updated_at:new Date()
  };
  titleNews:string='';
  firstParagraphe:string='';
  firstImage:ImageNews = {
    add_at:new Date(),
    alt_image: '',
    created_at: new Date(),
    id:0,
    lien_image:'',
    news_id:0,
    updated_at:new Date()
  };
  listTag:Tag[]=[];
  newsId:number=0;



  constructor(private newsService: NewsService) {
    this.firstImage.lien_image="";
   }

  ngOnInit(): void {

    this.newsService.getNewsDetailForCustomCard(this.newsWithoutDetails.id).subscribe((data:any)=>{
      this.newsId = this.newsWithoutDetails.id;
      this.titleNews = this.newsWithoutDetails.title;
      this.firstParagraphe = data['list-paragraphe'][0]['texte'];
      this.firstImage = data['list-image'][0];
      this.listTag = data['list-tag'];
      console.log("data for small show: ",data);

    });



  }

}
