import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomNewsCardComponent } from './custom-news-card.component';

describe('CustomNewsCardComponent', () => {
  let component: CustomNewsCardComponent;
  let fixture: ComponentFixture<CustomNewsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomNewsCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomNewsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
