import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomLocalNewsComponent } from './custom-local-news.component';

describe('CustomLocalNewsComponent', () => {
  let component: CustomLocalNewsComponent;
  let fixture: ComponentFixture<CustomLocalNewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomLocalNewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomLocalNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
