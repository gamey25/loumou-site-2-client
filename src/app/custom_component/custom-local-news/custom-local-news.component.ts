import { LocalVideo } from './../../interfaces/models';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-custom-local-news',
  templateUrl: './custom-local-news.component.html',
  styleUrls: ['./custom-local-news.component.scss']
})
export class CustomLocalNewsComponent implements OnInit {


  @Input()
  video:LocalVideo={
    add_at:new Date(),
    alt_video: '',
    created_at: new Date(),
    id:0,
    lien_video:'',
    site_id:0,
    updated_at:new Date()
  };

  urlSafe!: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer) { }

  ngOnInit(): void {

    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl( this.video.lien_video);
    console.log('la video a charger: ',this.video);
    
  }

}
