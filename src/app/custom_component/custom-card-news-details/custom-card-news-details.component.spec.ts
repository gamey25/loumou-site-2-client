import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCardNewsDetailsComponent } from './custom-card-news-details.component';

describe('CustomCardNewsDetailsComponent', () => {
  let component: CustomCardNewsDetailsComponent;
  let fixture: ComponentFixture<CustomCardNewsDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomCardNewsDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomCardNewsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
