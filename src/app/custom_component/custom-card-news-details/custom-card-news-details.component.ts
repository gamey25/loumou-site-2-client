import { LikeService } from './../../services/like.service';
import { CookieService } from 'ngx-cookie-service';
import { NewsService } from './../../services/news.service';
import { Component, Input, OnInit } from '@angular/core';
import { InteractionService } from 'src/app/services/interaction.service';

@Component({
  selector: 'app-custom-card-news-details',
  templateUrl: './custom-card-news-details.component.html',
  styleUrls: ['./custom-card-news-details.component.scss']
})
export class CustomCardNewsDetailsComponent implements OnInit {

  @Input()
  newsId:any;
  userId:number = 0;
  newsDetails:any;
  title:string="";
  firstParagraphe:string="";
  secondParagraphe:string="";
  thirdParagraphe:string="";
  fourthParagraphe:string="";

  firstLienImage:string="";
  secondLienImage:string="";
  thirdLienImage:string="";
  firstAltImage:string="";
  secondAltImage:string="";
  thirdAltImage:string="";

  numLike:number=0;
  numUnlike:number=0;
  like:boolean = true;

  userData:any;
  disableLikeUnlikeButton:boolean = true;
  showZone:string = "register";

  constructor(private newsService: NewsService,
              private cookieService: CookieService,
              private likeService: LikeService,
              private interactionService: InteractionService) { }

  ngOnInit(): void {

    this.newsService.getNewsDetailsById(this.newsId).subscribe((data:any)=>{
      this.newsDetails = data;
      this.title = data['news']['title'];
      this.firstParagraphe = data['list-paragraphes'][0]['texte'];
      this.secondParagraphe = data['list-paragraphes'][1]['texte'];
      this.thirdParagraphe = data['list-paragraphes'][2]['texte'];
      this.fourthParagraphe = data['list-paragraphes'][3]['texte'];

      this.firstLienImage = data['list-images'][0]['lien_image'];
      this.secondLienImage = data['list-images'][1]['lien_image'];
      this.thirdLienImage = data['list-images'][2]['lien_image'];

      this.firstAltImage = data['list-images'][0]['alt_image'];
      this.secondAltImage = data['list-images'][1]['alt_image'];
      this.thirdAltImage = data['list-images'][2]['alt_image'];

      this.numLike = data['number-like-and-unlike']['number-like'];
      this.numUnlike = data['number-like-and-unlike']['number-unlike'];
      this.getUserData();
      this.understandInteraction();
      console.log("les custom news donnees: ",this.newsDetails);
    });

  }

  understandInteraction(){
    this.interactionService.dataToShare$.subscribe((datToShare)=>{
      this.getUserData();
      console.log("data to share: ",datToShare);
    });
  }

  getUserData(){
    let user = this.cookieService.get('user-data');
    let isLike = this.cookieService.get('news-'+this.newsId);
      if(user == '' || isLike != ''){
        this.disableLikeUnlikeButton = true;
        console.log("this.cookieService.get: ",typeof(user));
      }else{
        this.disableLikeUnlikeButton = false;
        this.userData = JSON.parse(this.cookieService.get('user-data'));
        this.userId = this.userData['id'];
        console.log("this.userId: ",this.userId);
      }
  }

  onClickLikeButton(){
    this.like = true;
    this.likeService.postLikeComment({
      'user-id': this.userId,
      'news-id': this.newsId,
      'i_like': 1,
      'i_no_like': 0
    }).subscribe((data)=>{
      console.log("la data return du like est: ", data);
      if(data['isSuccess']  == true){
        this.cookieService.set('news-'+this.newsId, JSON.stringify("news-already-like"),{ expires: 1000, sameSite: 'Lax' });
        this.numLike = data["number-like-and-unlike"]["number-like"];
        this.numUnlike = data["number-like-and-unlike"]["number-unlike"];
        this.disableLikeUnlikeButton = true;
      }else{

      }
    });

  }

  onClickUnlikeButton(){
    this.like = false;
    this.likeService.postLikeComment({
      'user-id': this.userId,
      'news-id': this.newsId,
      'i_like': 0,
      'i_no_like': 1
    }).subscribe((data)=>{
      console.log("la data return du like est: ",data);
      if(data['isSuccess']  == true){
        this.cookieService.set('news-'+this.newsId, JSON.stringify("news-already-like"),{ expires: 1000, sameSite: 'Lax' });
        this.numLike = data["number-like-and-unlike"]["number-like"];
        this.numUnlike = data["number-like-and-unlike"]["number-unlike"];
        this.disableLikeUnlikeButton = true;
        console.log("already like: ",this.cookieService.get("news-"+this.newsId));
      }else{

      }
    });

  }

}
