import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomCarousselComponent } from './custom-caroussel.component';

describe('CustomCarousselComponent', () => {
  let component: CustomCarousselComponent;
  let fixture: ComponentFixture<CustomCarousselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomCarousselComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomCarousselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
