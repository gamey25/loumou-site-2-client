import { SiteService } from 'src/app/services/site.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-caroussel',
  templateUrl: './custom-caroussel.component.html',
  styleUrls: ['./custom-caroussel.component.scss']
})
export class CustomCarousselComponent implements OnInit {

  constructor(private siteService: SiteService) { }

  images:any = [];

  ngOnInit(): void {
    this.siteService.getAccueilData().subscribe((data:any)=>{
      this.images = data['image-caroussel'];

      

    });
  }

}
