import { SiteService } from 'src/app/services/site.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-footer',
  templateUrl: './custom-footer.component.html',
  styleUrls: ['./custom-footer.component.scss']
})
export class CustomFooterComponent implements OnInit {


  links:any;
  constructor(private siteService: SiteService) { }

  ngOnInit(): void {
    this.siteService.getSocialLinks().subscribe((data)=>{
      this.links = data;
      console.log("list link: ",data);
    });
  }

}
