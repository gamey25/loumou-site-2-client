
import { SiteService } from './services/site.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'deuxieme_site_web';

  constructor(private siteService: SiteService) { }

  ngOnInit(): void {
    this.siteService.getAccueilData().subscribe((data:any)=>{
      this.title = data['site-data'][0]['name'];
    });
  }

}
